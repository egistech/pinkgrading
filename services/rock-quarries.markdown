---
layout: service
title:  "Rock Quarries"
group: services
---
A quarry is a place from which dimension stone, rock, construction aggregate, riprap, sand, gravel, or slate has been excavated from the ground. Pink Grading offers quarry restoration and topsoil stripping solutions.

![Quarry](http://assets.pinkgrading.com/images/various/PC170018.jpg)