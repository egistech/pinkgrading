---
layout: page
title:  "Job Gallery"
---
Pink Grading, Inc. has over 40 years of work history, but here are just a few of our recently completed or ongoing projects.

## Crescent Rock Quarry

![Alternate text](http://assets.pinkgrading.com/images/jobsite/crescent-rock-quarry/crescent-rock-quarry_01.jpg)

![Alternate text](http://assets.pinkgrading.com/images/jobsite/crescent-rock-quarry/crescent-rock-quarry_04.jpg)
