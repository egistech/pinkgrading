---
layout: page
title:  "About Us"
---
Pink Grading, Inc. is a family owned and operated business that operates in the greater Omaha and Council Bluffs metropolitan areas. Gary L. Pink founded the company in 1970 as a one man operation that worked almost exclusively on smaller, residential projects. In the over 40 years since, he still serves as the President of the company joined now by his sons John and Daniel as Vice Presidents. 

The company grew from one employee to 25 year-round, full-time employees, and the projects moved from small, residential ones to large government and commercial projects on U.S. Highways and T.D. Ameritrade’s Omaha Headquarters. 

Pink Grading, Inc. offers a variety of earthmoving services including grading, excavation, dirt hauling, rock quarry, commercial and residential, and demolition work. The company offers additional full-time employment in the mid-summer peak for heavy equipment operators, CDL licensed drivers, and other qualified applicants.

For questions or contract estimates, contact our office...

![Alternate text](http://assets.pinkgrading.com/images/equipment-images/mac-triple-axel-dump-truck.jpg)